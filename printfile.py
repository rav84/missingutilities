import sys
try:
	if len(sys.argv) == 1:
		raise ValueError()
	filename = ""
	try:
		lines = int(sys.argv[1])
	except:
		filename = sys.argv[1]
		lines = 10
	if not filename:
		if len(sys.argv) == 3:
			filename = sys.argv[2]
		else:
			raise ValueError()
	f = open(filename)
	for l in range(lines):
		fl = f.readline()
		if fl:
			print '%3s' % (l+1),
			print ': ' + fl,
	f.close()
	print '---- End ----'
except:
	print 'Usage: printfile.py <no_of_lines> <filename>'
	print 'written by - ravish@arohanyas.com'