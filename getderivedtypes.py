import re
import sys

# Returns all dervied types of the given type. Only Searches in single file

if len(sys.argv) != 3:
    print("usage: python derived.py file baseClass")
    exit()

baseList = set([sys.argv[2]])
derivedTypes = set()

# Pattern Class bracket start
pB = "{"
extractPattern = f"(?<=class)\\s+([a-z][a-z_]*)\\s*(<[a-z,_\\s\\n<>]+>)?\\s*:([a-z<>,\\s\\n]+?)(?=(where|{pB}))"
compiledExtractPattern = re.compile(extractPattern, re.I | re.M)

with open(sys.argv[1], "r", encoding="utf-8") as file:
    data = file.read()
    typeInfo = compiledExtractPattern.findall(data)
    while len(baseList) > 0:
        baseType = baseList.pop()        
        matchPattern = f"[,\\s]+({baseType})[\\s,<]"
        compiledMatchPattern = re.compile(matchPattern, re.I | re.M)
        for tInfo in typeInfo:
            baseTypes = tInfo[2]
            match = compiledMatchPattern.search(baseTypes)
            if match:
                dt = tInfo[0].strip()
                if dt not in derivedTypes:
                    baseList.add(dt)
                    derivedTypes.add(dt)


for dt in derivedTypes:
    print(dt)
